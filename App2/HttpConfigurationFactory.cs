﻿using System.Web.Http;
using System.Web.Http.Dispatcher;

namespace App2
{
    public class HttpConfigurationFactory
    {

        public static HttpConfiguration Configure(string pipeline = null)
        {
            var routeTemplate = "api/{controller}/{id}";

            if (string.IsNullOrEmpty(pipeline) == false)
            {
                routeTemplate = pipeline + "/" + routeTemplate;
            }

            var httpconfigApp2 = new HttpConfiguration();
            httpconfigApp2.Routes.MapHttpRoute(
                    name: "DefaultApiApp2",
                    routeTemplate: routeTemplate,
                    defaults: new { controller = "values", id = RouteParameter.Optional });

            httpconfigApp2.Services.Replace(typeof(IAssembliesResolver), new App2AssemblyResolver());

            return httpconfigApp2;
        }

    }
}
