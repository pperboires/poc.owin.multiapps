﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Dispatcher;

namespace App2
{
    public class App2AssemblyResolver : DefaultAssembliesResolver
    {
        public override ICollection<Assembly> GetAssemblies()
        {
            //var baseAssemblies = base.GetAssemblies();
            //var assemblies = new List<Assembly>(baseAssemblies);
            
            var assemblies = new List<Assembly>();

            // Add whatever additional assemblies you wish
            assemblies.Add(this.GetType().Assembly);

            return assemblies;
        }
    }

}
