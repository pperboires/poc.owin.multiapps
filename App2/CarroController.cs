﻿using System.Web.Http;

namespace App2
{
    public class CarroController : ApiController
    {
        [HttpGet]
        public Carro Get()
        {
            return new Carro
            {
                Ano = 2013,
                Marca = "Toyota",
                Modelo = "Corolla"
            };
        }
    }
}