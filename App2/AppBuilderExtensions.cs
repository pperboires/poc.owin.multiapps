﻿using Owin;

namespace App2
{
    public static class AppBuilderExtensions
    {
        public static IAppBuilder ConfigureApp2(this IAppBuilder appBuilder, string pipeline = null)
        {
            return appBuilder.UseWebApi(HttpConfigurationFactory.Configure(pipeline));
        }
    }
}
