﻿using System;
using Microsoft.Owin.Hosting;

namespace App2.SelfHost
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            const string url = "http://localhost:7777";
            using (WebApp.Start<Startup>(url))
            {
                Console.WriteLine("\n\nApplication 2...");
                Console.WriteLine("\n\nServer listening at {0}. Press enter to stop", url);
                Console.ReadLine();
            }
        }
    }
}