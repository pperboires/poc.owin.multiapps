﻿using Owin;

namespace App2.SelfHost
{
    public class Startup
    {
        // Invoked once at startup to configure your application.
        public void Configuration(IAppBuilder app)
        {
            app.ConfigureApp2();
        }
    }
}