﻿using App1;
using App2;
using Owin;

namespace Host
{
    public class Startup
    {
        // Invoked once at startup to configure your application.
        public void Configuration(IAppBuilder app)
        {
            app.ConfigureApp1("app1");
            app.ConfigureApp2("app2");
        }
    }
}