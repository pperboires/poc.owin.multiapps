﻿using System.Collections.Generic;
using App1.Domain.Entities;

namespace App1.Domain.Repositories
{
    public interface ICarroRepository
    {
        Carro Get(int id);
        IEnumerable<Carro> GetAll();
    }
}
