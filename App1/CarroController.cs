﻿using System.Collections.Generic;
using System.Web.Http;
using App1.Domain.Entities;
using App1.Domain.Repositories;

namespace App1
{
    [RoutePrefix("/Carro")]
    public class CarroController : ApiController
    {

        private readonly ICarroRepository _carroRepository;

        public CarroController(ICarroRepository carroRepository)
        {
            _carroRepository = carroRepository;
        }

        [HttpGet]
        [Route("/Get/{id}")]
        public Carro Get(int id)
        {
            return _carroRepository.Get(id);
        }
        
        [HttpGet]
        [Route("/All")]
        public IEnumerable<Carro> All()
        {
            return _carroRepository.GetAll();
        }

    }
}