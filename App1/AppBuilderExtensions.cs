﻿using System.Web.Http;
using App1.Domain.Repositories;
using App1.Persistence.Memory;
using Autofac;
using Autofac.Integration.WebApi;
using Owin;

namespace App1
{
    public static class AppBuilderExtensions
    {
        // http://autofac.readthedocs.org/en/latest/integration/webapi.html#register-controllers
        
        public static IAppBuilder ConfigureApp1(this IAppBuilder appBuilder, string pipeline = null)
        {
            // STANDARD WEB API SETUP:

            // Get your HttpConfiguration. In OWIN, you'll create one
            // rather than using GlobalConfiguration.
            var httpConfig = HttpConfigurationFactory.Configure(pipeline);

            var builder = new ContainerBuilder();

            // OWIN WEB API SETUP:
            builder.RegisterApiControllers(typeof(AppBuilderExtensions).Assembly);
            //builder.RegisterType<CarroController>().InstancePerRequest();


            builder.RegisterType<CarroRepositoryMemory>().As<ICarroRepository>();

            var container = builder.Build();

            httpConfig.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            // Register the Autofac middleware FIRST, then the Autofac Web API middleware,
            // and finally the standard Web API middleware.
            appBuilder.UseAutofacMiddleware(container);
            appBuilder.UseAutofacWebApi(httpConfig);
            appBuilder.UseWebApi(httpConfig);

            return appBuilder;
        }

    }
}
