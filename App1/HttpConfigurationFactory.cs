﻿using System.Web.Http;
using System.Web.Http.Dispatcher;

namespace App1
{
    public class HttpConfigurationFactory
    {

        public static HttpConfiguration Configure(string pipeline = null)
        {
            var routeTemplate = "api/{controller}/{action}/{id}";

            if (string.IsNullOrEmpty(pipeline) == false)
            {
                routeTemplate = pipeline + "/" + routeTemplate;
            }

            var httpconfig = new HttpConfiguration();
            httpconfig.Formatters.Remove(httpconfig.Formatters.XmlFormatter);

            httpconfig.Routes.MapHttpRoute(
                    name: "DefaultApiApp1",
                    routeTemplate: routeTemplate,
                    defaults: new { controller = "values", id = RouteParameter.Optional });

            httpconfig.Services.Replace(typeof(IAssembliesResolver), new App1AssemblyResolver());

            return httpconfig;
        }

    }
}
