﻿using System.Collections.Generic;
using System.Linq;
using App1.Domain.Entities;
using App1.Domain.Repositories;

namespace App1.Persistence.Memory
{
    public class CarroRepositoryMemory : ICarroRepository
    {
        private readonly IList<Carro> _carros = new List<Carro>();

        public CarroRepositoryMemory()
        {
            _carros.Add(new Carro
            {
                Id = 1,
                Ano = 1976,
                Marca = "Chevrolet",
                Modelo = "Opala SS 4100"
            });

            _carros.Add(new Carro
            {
                Id = 2,
                Ano = 1961,
                Marca = "Volkswagen",
                Modelo = "Fusca"
            });

            _carros.Add(new Carro
            {
                Id = 3,
                Ano = 1974,
                Marca = "Ford",
                Modelo = "Maverick GT"
            });

            _carros.Add(new Carro
            {
                Id = 4,
                Ano = 1982,
                Marca = "Ford",
                Modelo = "Del Rey Ouro"
            });

            _carros.Add(new Carro
            {
                Id = 5,
                Ano = 1977,
                Marca = "Miura",
                Modelo = "Sport"
            });
        }

        public Carro Get(int id)
        {
            return _carros.AsQueryable().FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Carro> GetAll()
        {
            return _carros;
        }
    }
}