﻿using System;
using Microsoft.Owin.Hosting;

namespace App1.SelfHost
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            const string url = "http://localhost:8888";
            using (WebApp.Start<Startup>(url))
            {
                Console.WriteLine("\n\nApplication 1...");
                Console.WriteLine("\n\nServer listening at {0}. Press enter to stop", url);
                Console.ReadLine();
            }
        }
    }
}